export interface Contact {
    id: number;
    name: string;
    avatar: string;
    email: string;
    phone: string;
    address: ContactAddress;
    deleted: boolean;
    createdAt: number;
}
export interface ContactAddress {
    street: string;
    city: string;
    country: string;
}