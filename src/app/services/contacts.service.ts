import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Contact } from './models/contact.model'

@Injectable({
    providedIn: 'root'
})
export class ContactService {
    private _contact: Contact[] = [];
    private _error: string = "";

    //dependancy Injection
    constructor(private readonly http: HttpClient) {

    }


    public fetchContacts(): void {
        //observable
        this.http.get<Contact[]>('http://localhost:3000/contact')
            .subscribe(
                (contacts: Contact[]) => {
                    this._contact = contacts;
                },
                (error: HttpErrorResponse) => {
                    this._error = error.message;
                }
            )
    }

    public contacts(): Contact [] {
        return this._contact;
    }
    public error(): string {
        return this._error
    }
}