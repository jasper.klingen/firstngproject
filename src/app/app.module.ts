import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http'
import { ContactSelectedComponent } from './Components/contact-selected/contact-selected.component';
import { ContactsPage } from './contacts/contacts.page';
import { AppRoutingModule } from './app-routing.module';
import { ContactListComponent } from './Components/contact-list/contact-list.component';
import { ContactListItemComponent } from './Components/contact-list-item/contact-list-item.component';
import { ContactCreatePage } from './contact-create/contact-create.page';

@NgModule({ //decorator
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactSelectedComponent,
    ContactListItemComponent,
    ContactsPage,
    ContactCreatePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
