import { Component } from '@angular/core'
import { SelectedContactService } from '../../services/selected-contact.service';
import { Contact } from '../../services/models/contact.model'
@Component({
    selector: 'app-contact-selected',
    templateUrl: './contact-selected.component.html',
    styleUrls: ['./contact-selected.component.css',]

})
export class ContactSelectedComponent {
    constructor(private readonly selectedContactService: SelectedContactService) {

    }
    get contact(): Contact | null {
        return this.selectedContactService.contact();
    }
}