import { Component, OnInit } from '@angular/core'
import { ContactService } from 'src/app/services/contacts.service';
import { Contact } from 'src/app/services/models/contact.model';
import { SelectedContactService } from 'src/app/services/selected-contact.service';


@Component({
    selector: 'app-contact-list',
    templateUrl: './contact-list.component.html',
    styleUrls: ['./contact-list.component.css']
})

//Decorator
export class ContactListComponent implements OnInit {
    constructor(
        private readonly contactService: ContactService,
        private readonly selectedContactService: SelectedContactService
    ) {
    }
    ngOnInit(): void {
        this.contactService.fetchContacts();
    }
    get contacts(): Contact[] {
        return this.contactService.contacts();
    }
    public handleContactClicked(contact: Contact): void {
        this.selectedContactService.setContact(contact);
    }
}

