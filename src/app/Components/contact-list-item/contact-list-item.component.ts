import {Component, Input, Output, EventEmitter} from '@angular/core';
import { Contact } from 'src/app/services/models/contact.model';
import { ContactListComponent } from '../contact-list/contact-list.component';


@Component({
    selector: 'app-contact-list-item',
    templateUrl: './contact-list-item.component.html',
    styleUrls: ['./contact-list-item.component.css']
})
export class ContactListItemComponent{
    @Input() contact: Contact | undefined;
    @Output() clicked: EventEmitter<Contact> = new EventEmitter()
    
    public onContactClicked(): void{
        this.clicked.emit(this.contact);
    }
}